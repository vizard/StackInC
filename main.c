#include <stdio.h>
#include <stdlib.h>


typedef int data;

struct stack
{
  int top;
  int size;
  data *stack;
};

typedef struct stack stack;

void push (stack *st, data d);
data pop (stack *st);
void printstack (stack *st);

/** Stack in C
  * stack - stack type
  * data - type of data stored in stack elements
  * pop - return first element in stack *st
  * push - put element d on the top of stack *st
  */

int
main (void)
{
  stack st = {.top = -1, .size = 0, .stack = NULL};
  int a;
  while ((scanf ("%d", &a), a)!=-1)
    push(&st, a);
  printf("Full stack is: \n");
  printstack(&st);
  printf("Pop: \n");
  while (a = pop(&st))
    printf ("%d ", a);
  printf("\n");
  return 0;
}

void
push (stack *st, data d)
{
  if (st->size == st->top+1)
  {
    st->size = (st->size > 0) ? st->size * 2 : 1;
    st->stack = realloc (st->stack, st->size * sizeof (stack));
  }
  (st->stack)[++(st->top)] = d;
}

data
pop (stack *st)
{
  if (st->top == -1)
    return NULL;
  else
    return (st->stack)[(st->top)--];
}

void
printstack (stack *st)
{
  for (int i = st->top; i >= 0; i--)
    printf ("%d ", st->stack[i]);
  printf("\n");
}
